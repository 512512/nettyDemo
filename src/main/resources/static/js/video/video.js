$.ajaxSetup({
    dataType : 'jsonp',
});

const vo=new Vue({
    data:{
        jsession:null,
        isInitFinished:false
    }
})
/**
 * 用户登录
 * @param username
 * @param password
 */
var login=function (username,password) {
    if(username!=null&&password!=null){

        $.ajax(
            {
                type:'get',
                url : `http://120.25.214.26/StandardApiAction_login.action?account=${username}&password=${password}`,
                success  : function(data) {
                    let {jsession,result}=data;
                    if(result==0){
                        vo.jsession=jsession;
                        //$("#divPlugin").append(html);
                        //playVideo()
                        getLng();
                    }else{
                        alert(`登录失败错误代码:${result}`);
                    }
                },
                error : function() {
                    alert('登录失败');
                }
            }
        );
    }
}
/**
 * 用户注销
 * @param jsession
 */
var logout=function (jsession) {

    if(jsession){
        $.ajax(
            {
                type:'get',
                url : `http://120.25.214.26/StandardApiAction_logout.action?jsession=${jsession}`,
                success  : function(data) {
                    console.log(data);
                    reSetVideo()
                }
            }
        );
    }
}

//初始化视频插件
var initPlayerExample=function () {
    //视频插件初始化参数
    let params = {
        allowFullscreen: "true",
        allowScriptAccess: "always",
        bgcolor: "#FFFFFF",
        wmode: "transparent"
    };
    //初始化flash
    swfobject.embedSWF("http://120.25.214.26/808gps/open/player/player.swf", "cmsv6flash", 550, 320, "11.0.0", null, null, params, null);
    initFlash();
}
//视频插件是否加载完成
var initFlash=function () {
    if (swfobject.getObjectById("cmsv6flash") == null ||
        typeof swfobject.getObjectById("cmsv6flash").setWindowNum == "undefined" ) {
        setTimeout(initFlash, 50);
    } else {
        //设置视频插件的语言
        swfobject.getObjectById("cmsv6flash").setLanguage("http://120.25.214.26/808gps/open/player/cn.xml");
        //先将全部窗口创建好
        swfobject.getObjectById("cmsv6flash").setWindowNum(36);
        //再配置当前的窗口数目
        swfobject.getObjectById("cmsv6flash").setWindowNum(4);
        //设置视频插件的服务器
        swfobject.getObjectById("cmsv6flash").setServerInfo("120.25.214.26", "6605");
        vo.isInitFinished = true;
    }
}
//播放视频
function playVideo() {
    let html=`http://120.25.214.26/808gps/open/hls/index.html?lang=zh&vehiIdno=50960&account=xinpai&password=000000&channel=0`;
    $("#divPlugin").attr("src",html);
}
//重置视频窗口
function reSetVideo() {
    swfobject.getObjectById("cmsv6flash").reSetVideo(0);
}
//停止播放视频
function stopVideo() {
    $("#divPlugin").attr("src","");
}
//获取gps信息
function getLng() {
    $.ajax(
        {
            type:'get',
            url : `http://120.25.214.26/StandardApiAction_getDeviceStatus.action?jsession=${vo.jsession}&devIdno=50960&toMap=2`,
            success  : function(data) {

                //var gps =data.status[0].lng+","+data.status[0].lat;
                //var gps =new AMap.LngLat(data.status[0].mlng,data.status[0].mlat);
                var gps=[parseFloat(data.status[0].mlng),parseFloat(data.status[0].mlat)]
                console.log(gps);
                AMap.convertFrom(gps, 'baidu', function (status, info) {
                    console.log(info);
                    if (info.info === 'ok') {
                        var lnglats = info.locations[0]; // Array.<LngLat>
                        onComplete(lnglats)
                    }
                });
/*                var url=`http://restapi.amap.com/v3/assistant/coordinate/convert?locations=${gps}&coordsys=gps&output=xml&key=bfc46b58e15af04041da2831f464d6a3`
                console.log(url);*/
/*                $.ajax(
                    {
                        type:'get',
                        url : `http://restapi.amap.com/v3/assistant/coordinate/convert?locations=${gps}&coordsys=baidu&output=JSON&key=bfc46b58e15af04041da2831f464d6a3`,
                        success  : function(re) {
                            console.log(re);
                            if (re.info === 'ok') {
                                var lnglats = re.locations; // Array.<LngLat>
                                // 构造点标记
                                map.panTo(lnglats);
/!*                                                        AMap.event.addListener(lnglats, 'complete', onComplete);//返回定位信息
                                                        AMap.event.addListener(lnglats, 'error', onError);      //返回定位出错信息*!/
                                onComplete(lnglats)
                            }
                        }
                    }
                );*/
            }
        }
    );
}
//解析定位结果
function onComplete(data) {
    console.log(data.position);
    //map.setCenter(data.position);
    marker = new AMap.Marker({
        icon: "../img/g.png",
        map:map,
        position: data
    });
    marker.on( "click", function () {
        $('#myModal').modal('show')
        playVideo()
    })
    var str=['定位成功'];
    str.push('经度：' + data.getLng());
    str.push('纬度：' + data.getLat());
    str.push('是否经过偏移：' + (data.isConverted ? '是' : '否'));
    document.getElementById('tip').innerHTML = str.join('<br>');
    map.setFitView();
}
//解析定位错误信息
function onError(data) {
    document.getElementById('tip').innerHTML = '定位失败';
}