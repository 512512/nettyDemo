package com.zw;

import com.zw.netty.Server.NettyServer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;

@SpringBootApplication
public class NettyApplication implements CommandLineRunner {

	@Resource
	private NettyServer nettyServer;

	public static void main(String[] args) {
		SpringApplication.run(NettyApplication.class, args);
	}

	@Bean
	public NettyServer nettyServer(){
		return new NettyServer();
	}

	@Override
	public void run(String... args) throws Exception {
		nettyServer.start();

	}
}
