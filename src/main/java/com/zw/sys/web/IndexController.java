package com.zw.sys.web;

import com.zw.common.ChatConstants;
import com.zw.sys.Service.DeviceService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 创建人:zhuwei
 * 邮箱:121203654@qq.com
 * 创建时间:2018-04-26 15:59
 */
@Controller
public class IndexController{

    @Resource
    private DeviceService deviceService;
        // 跳转到交谈聊天页面
        @RequestMapping(value = "/", method = RequestMethod.GET)
        public String talk() {
            return "webSocket";
        }
    @RequestMapping(value = "/video", method = RequestMethod.GET)
    public String video() {
        return "video";
    }
        @RequestMapping(value = "getUsers",method = RequestMethod.POST)
        public @ResponseBody Object findUsers(){
            return deviceService.selectList(null);
        }
}
