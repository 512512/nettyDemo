package com.zw.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Controller
@RequestMapping("/deviceGroup")
public class DeviceGroupController {

}

