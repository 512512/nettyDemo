package com.zw.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Data
@NoArgsConstructor
public class DeviceGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private String description;

    @Override
    public String toString() {
        return "DeviceGroup{" +
        ", id=" + id +
        ", name=" + name +
        ", description=" + description +
        "}";
    }
}
