package com.zw.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Data
@NoArgsConstructor
public class DeviceData implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String serial;
    private Date time;
    private String log;
    private Integer type;

    public DeviceData(String serial, Date date, String currentCmd, Integer key) {
        this.serial=serial;
        this.time=date;
        this.log=currentCmd;
        this.type=key;
    }


    @Override
    public String toString() {
        return "DeviceData{" +
        ", id=" + id +
        ", serial=" + serial +
        ", time=" + time +
        ", log=" + log +
        ", type=" + type +
        "}";
    }
}
