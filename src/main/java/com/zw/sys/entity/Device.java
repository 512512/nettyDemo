package com.zw.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Data
@NoArgsConstructor
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 设备ID
     */
    private String serial;
    /**
     * 描述
     */
    private String description;
    private String currentCmd;
    /**
     * 设备状态
     */
    private Integer deviceStatusId;
    /**
     * 更新时间
     */
    private Date lastHeartbeatDate;
    /**
     * 设备类型
     */
    private String building;
    /**
     * 地里位置
     */
    private String location;
    /**
     * 心率
     */
    private Integer heartRate;
    /**
     * 步数
     */
    private Integer stepNumber;
    /**
     * 血压
     */
    private transient String pressure;

    private transient  String channelId;
    @Override
    public String toString() {
        return "Device{" +
        ", id=" + id +
        ", name=" + name +
        ", serial=" + serial +
        ", description=" + description +
        ", currentCmd=" + currentCmd +
        ", deviceStatusId=" + deviceStatusId +
        ", lastHeartbeatDate=" + lastHeartbeatDate +
        ", building=" + building +
        ", location=" + location +
        ", heartRate=" + heartRate +
        ", stepNumber=" + stepNumber +
        "}";
    }
}
