package com.zw.sys.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Data
public class DeviceStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;

    @Override
    public String toString() {
        return "DeviceStatus{" +
        ", id=" + id +
        ", name=" + name +
        "}";
    }
}
