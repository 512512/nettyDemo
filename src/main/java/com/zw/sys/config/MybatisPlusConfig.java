package com.zw.sys.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 创建人:zhuwei
 * 邮箱:121203654@qq.com
 * 创建时间:2018-05-03 11:25
 */
@Configuration
@MapperScan("com.zw.sys.mapper*")
public class MybatisPlusConfig {
}
