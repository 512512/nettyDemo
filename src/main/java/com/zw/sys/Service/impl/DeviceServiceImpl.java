package com.zw.sys.Service.impl;

import com.zw.sys.Service.DeviceService;
import com.zw.sys.entity.Device;
import com.zw.sys.mapper.DeviceMapper;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Service("deviceService")
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements DeviceService {

}
