package com.zw.sys.Service.impl;

import com.zw.sys.Service.DeviceGroupService;
import com.zw.sys.entity.DeviceGroup;
import com.zw.sys.mapper.DeviceGroupMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Service
public class DeviceGroupServiceImpl extends ServiceImpl<DeviceGroupMapper, DeviceGroup> implements DeviceGroupService {

}
