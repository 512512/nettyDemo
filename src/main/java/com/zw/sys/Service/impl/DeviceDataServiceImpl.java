package com.zw.sys.Service.impl;

import com.zw.sys.Service.DeviceDataService;
import com.zw.sys.entity.DeviceData;
import com.zw.sys.mapper.DeviceDataMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
@Service("deviceDataService")
public class DeviceDataServiceImpl extends ServiceImpl<DeviceDataMapper, DeviceData> implements DeviceDataService {

}
