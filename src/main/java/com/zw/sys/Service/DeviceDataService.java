package com.zw.sys.Service;

import com.zw.sys.entity.DeviceData;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
public interface DeviceDataService extends IService<DeviceData> {

}
