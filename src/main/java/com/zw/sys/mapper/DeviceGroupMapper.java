package com.zw.sys.mapper;

import com.zw.sys.entity.DeviceGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 朱伟
 * @since 2018-05-03
 */
public interface DeviceGroupMapper extends BaseMapper<DeviceGroup> {

}
