package com.zw.netty.Server.Handler;

import io.netty.channel.Channel;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.EventExecutor;
import io.netty.util.internal.PlatformDependent;

import java.util.concurrent.ConcurrentMap;

/**
 * 创建人:zhuwei
 * 邮箱:121203654@qq.com
 * 创建时间:2018-05-04 15:58
 */
public class ZwChannelGroup extends DefaultChannelGroup {
    private static final ConcurrentMap<String, Channel> channels = PlatformDependent.newConcurrentHashMap();

    public ZwChannelGroup(EventExecutor executor) {
        super(executor);
    }

    public ZwChannelGroup(String name, EventExecutor executor) {
        super(name, executor);
    }

    public ZwChannelGroup(EventExecutor executor, boolean stayClosed) {
        super(executor, stayClosed);
    }

    public ZwChannelGroup(String name, EventExecutor executor, boolean stayClosed) {
        super(name, executor, stayClosed);
    }

    public void add(Channel channel,String deviceId){
        System.out.println(deviceId);
        System.out.println("是否存在key"+!channels.containsKey(deviceId));
        if(!channels.containsKey(deviceId)){
            channels.put(deviceId,channel);
            add(channel);
        }else if(channels.containsKey(deviceId)&&!channels.containsValue(channel)){
            System.out.println("发现重复设备去除");
            Channel c=channels.get(deviceId);
            remove(c);
            add(channel);
            channels.put(deviceId,channel);
        }
    }
}
