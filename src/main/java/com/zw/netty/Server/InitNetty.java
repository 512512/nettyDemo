package com.zw.netty.Server;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/*@Component*/
public class InitNetty implements ApplicationListener<ContextRefreshedEvent>,Ordered {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        start();
    }

    private void start() {
        new NettyServer().start();
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
