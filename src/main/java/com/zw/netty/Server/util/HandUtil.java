package com.zw.netty.Server.util;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.text.StrSpliter;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.zw.sys.entity.Device;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

/**
 * 创建人:zhuwei
 * 邮箱:121203654@qq.com
 * 创建时间:2018-04-26 16:48
 */
public class HandUtil {

    public static String GetDateString(Date date,String format){
        SimpleDateFormat sf=new SimpleDateFormat(format);
        return sf.format(date);
    }

    public static String[] GetStringArray(String s){
        if(s!=null){
            return s.split(",");
        }
        return null;
    }

    public static String joinParam(Object... argArray){
        return StrFormatter.format("@G#@,V01,{},@R#@",argArray);
    }

    public static Optional<Device> CreateDevice(String str){
        String[] strings= str.split(",");
        Device device=new Device();
        device.setSerial(strings[3]);
        device.setCurrentCmd(str);
        if(strings[2].equals("6")){
            device.setLocation(strings[6].replace(";",","));
            device.setCurrentCmd(device.getLocation());
        }else if(strings[2].equals("43")){
            HashMap<String, Object> paramMap = new HashMap<>();
            //String[] sst=strings[9].split("|");
            String[] ssss=strings[9].replace("|","#").split("#");
            //System.out.println(StringUtils.join(sst, ";"));
            for (int i = 0; i < ssss.length; i++) {
                String[] s = ssss[i].split(";");
                ssss[i]=s[0]+","+s[2];
            }
            paramMap.put("cl",strings[7].replace(";",",").replace("|",";"));
            paramMap.put("wl",StringUtils.join(ssss, ";"));
            paramMap.put("coord","gcj02");
            paramMap.put("output","json");
            JSONObject jsonObject=JSONObject.parseObject(HttpUtil.get("http://api.cellocation.com:81/loc/",paramMap));
            device.setLocation(jsonObject.getString("lat")+","+jsonObject.getString("lon"));
            device.setCurrentCmd(device.getLocation());
        }
        return Optional.of(device);
    }

}
